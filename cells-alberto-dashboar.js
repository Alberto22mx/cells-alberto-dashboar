{
  const {
    html,
  } = Polymer;
  /**
    `<cells-alberto-dashboar>` Description.

    Example:

    ```html
    <cells-alberto-dashboar></cells-alberto-dashboar>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-alberto-dashboar | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsAlbertoDashboar extends Polymer.Element {

    static get is() {
      return 'cells-alberto-dashboar';
    }

    static get properties() {
      return {
        users: {
          type: Object,
          value: [
            {
              img: '../images/dragon.jpeg',
              name: 'Oscar',
              last: 'Gonzalez',
              address: 'Lomas Estrella',
              hobbies: ['Videojuegos', 'Futbol', 'Dormir']
            },
            {
              img: 'http://placehold.it/150x150/000000/FFFFFF',
              name: 'Luis',
              last: 'Kelly Díaz',
              address: 'Una casa',
              hobbies: 'Programar'
            },
            {
              img: '../images/tresmonos.jpeg',
              name: 'Jesús',
              last: 'Guzmán Vitte',
              address: 'Calle Rancho La Laguna 46B, Colonia Fraccionamiento San Antonio, Cuautitlán Izcalli, Estado de México',
              hobbies: 'Wu-Shu'
            },
            {
              img: '../images/gato-agosto.jpg',
              name: 'Iris',
              last: 'Lizeth',
              address: 'Puerto Yavaros',
              hobbies: ['Ver peliculas y series', 'salir con amigos y familiares']
            },
            {
              img: '../images/migato.jpg',
              name: 'Jhony Fernando',
              last: 'Bartolo Diaz',
              address: 'Tlalpan Centro, Calle Francinso I Madero',
              hobbies: 'Salir de viaje'
            },
            {
              img: '../images/favicon.ico',
              name: 'Silvino',
              last: 'Piza Gaspar',
              address: 'Zumpango, Estado de México',
              hobbies: ['GYM', 'DIBUJAR', 'ESCUCHAR MÚSICA', 'JUGAR VIDEOJUEGOS', 'SALIR AL CINE']
            },
            {
              img: '../images/avatar.jpg',
              name: 'Victor Manuel',
              last: 'Roman Orozco',
              address: 'sur 119 Iztcalco, CDMX',
              hobbies: 'Futbol'
            },
            {
              img: '../images/icon-72x72.png',
              name: 'Ernesto',
              last: 'Mejia Camacho',
              address: 'calzada de la primera #163. tlahuac',
              hobbies: ['tocar la guitarra', 'jugar video juegos', 'hacer deporte']
            },
            {
              img: '../images/leon.jpeg',
              name: 'Luis Enrique',
              last: 'Ruiz Ruiz',
              address: 'Calle Adios #274',
              hobbies: ['Leer', 'jugar video juegos']
            },
            {
              img: '../images/cara.jpg',
              name: 'Ruben',
              last: 'de la Cruz',
              address: 'Calle 4 tetelpan',
              hobbies: ['Series', 'Musica', 'Jugar videjuegos']
            },
            {
              img: '../images/imagen1.jpeg',
              name: 'Diego',
              last: 'Vazquez Alvarez',
              address: 'av. jazmin 32 a',
              hobbies: 'ir al Cine'
            },
            {
              img: '../images/tony.jpg',
              name: 'Tony',
              last: '',
              address: '',
              hobbies: ['Programar', 'Netflix']
            },
            {
              img: '../images/mon.png',
              name: 'Monica',
              last: 'Rivera Valle',
              address: 'Chimalhuacán, Estado de México',
              hobbies: 'Ver series, películas'
            },
            {
              img: '../images/PkVweyp.jpg',
              name: 'Martin',
              last: 'Juarez',
              address: 'Tlaltenco Tlahuac',
              hobbies: ['leer', 'deportes', 'musica']
            },
            {
              img: '../images/favicon.ico',
              name: 'Alberto',
              last: 'Serrano',
              address: 'Santiago',
              hobbies: 'Programar'
            },
            {
              img: '',
              name: 'memo',
              last: 'Ramirez',
              address: 'toledo col juarez',
              hobbies: ''
            }
          ]
        },
        user: {
          type: String,
          notify: true,
          value: "",
          observer: 'cargar'
        }
      };
    }
    myfilter(item){
      let name = item.name;
      return name.indexOf(this.user) != -1;
    }
    cargar(){
      Polymer.dom(this.root).querySelector("#users").render();
    }
    // filter="myfilter"
    static get template() {
      return html `
      <style include="cells-alberto-dashboar-styles cells-alberto-dashboar-shared-styles"></style>
      <slot></slot>
      <input type="text" value="{{user::input}}">
      <template is="dom-repeat" items="{{users}}" filter="myfilter" id="users">
          <div class="card">
          {{item.name}}
          {{item.last}}
          </div>
      </template>
      `;
    }
  }

  customElements.define(CellsAlbertoDashboar.is, CellsAlbertoDashboar);
}